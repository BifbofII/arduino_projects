// Main source file for a arduino based, simulated bomb for a real-life CS game

#include <string.h>
#include <LiquidCrystal.h>
#include <Keypad.h>
#include <TimerOne.h>

// Pins
#define BUZZER_PIN A0
#define GREEN_PIN 1
#define YELLOW_PIN 0
#define RED_PIN 12

// LCD
#define LCD_RESET A1
#define LCD_RW A2
#define LCD_ENABLE A3
#define LCD_D4 11
#define LCD_D5 A4
#define LCD_D6 A5
#define LCD_D7 10
LiquidCrystal lcd(LCD_RESET, LCD_RW, LCD_ENABLE, LCD_D4, LCD_D5, LCD_D6, LCD_D7);

// Keypad
const byte ROWS = 4;
const byte COLS = 4;
const char KEYS[ROWS][COLS] = {
    {'1', '2', '3', 'A'},
    {'4', '5', '6', 'B'},
    {'7', '8', '9', 'C'},
    {'*', '0', '#', 'D'}};
const byte ROW_PINS[ROWS] = {2, 3, 4, 5};
const byte COL_PINS[COLS] = {6, 7, 8, 9};
Keypad keypad = Keypad(makeKeymap(KEYS), ROW_PINS, COL_PINS, ROWS, COLS);

// Timer tick (in us) and runtime (in ticks)
#define TICK 10000
#define RUN_TIME 4000

// Buzzer
#define BUZZER_INT 50
#define BUZZER_FREQ 440

// Defined codes
#define CODE_LENGTH 7
const char ARM_CODE[CODE_LENGTH + 1] = "7355608";
const char DIFFUSE_CODE[CODE_LENGTH + 1] = "7355608";
const char RESET_CODE[CODE_LENGTH + 1] = "0000000";

// Bomb state
enum state_t
{
  INIT,
  ARMED,
  BLOWN,
  DISARMED,
  ENDED
};

// Global variables
state_t state = INIT;
char currentCode[CODE_LENGTH + 1] = "";
uint16_t timer = 0;

// Function prototypes
void timerISR();

void setup()
{
  pinMode(GREEN_PIN, OUTPUT);
  pinMode(YELLOW_PIN, OUTPUT);
  pinMode(RED_PIN, OUTPUT);
  keypad.addEventListener(keypadEventHandler);

  Timer1.initialize(TICK);
  Timer1.attachInterrupt(timerISR);
  Timer1.stop();

  lcd.begin(16, 2);
  displayInitialized();
  lcd.setCursor(0, 1);
  lcd.print("Code:");
}

void loop()
{
  static char buf[7];
  // Process key presses
  keypad.getKey();
  // Print current time
  if (state == ARMED)
  {
    lcd.setCursor(9, 0);
    dtostrf(timer / 1000000. * TICK, 6, 2, buf);
    if (timer / 1000000. * TICK < 5)
    {
      if ((timer / (BUZZER_INT / 4)) % 2)
        tone(BUZZER_PIN, BUZZER_FREQ * 4);
      else
        noTone(BUZZER_PIN);
    }
    else if (timer / 1000000. * TICK < 20)
    {
      if ((timer / (BUZZER_INT / 2)) % 2)
        tone(BUZZER_PIN, BUZZER_FREQ * 2);
      else
        noTone(BUZZER_PIN);
    }
    else
    {
      if ((timer / BUZZER_INT) % 2)
        tone(BUZZER_PIN, BUZZER_FREQ);
      else
        noTone(BUZZER_PIN);
    }
    lcd.print(buf);
  }
  else if (state == BLOWN)
  {
    displayBlown();
    state = ENDED;
  }
}

inline void displayInitialized()
{
  lcd.setCursor(0, 0);
  lcd.print("Spike ready     ");
  digitalWrite(GREEN_PIN, 1);
  digitalWrite(YELLOW_PIN, 0);
  digitalWrite(RED_PIN, 0);
  noTone(BUZZER_PIN);
}

inline void displayArmed()
{
  lcd.setCursor(0, 0);
  lcd.print("Armed:         s");
  digitalWrite(GREEN_PIN, 0);
  digitalWrite(YELLOW_PIN, 1);
  digitalWrite(RED_PIN, 0);
}

inline void displayBlown()
{
  lcd.setCursor(0, 0);
  lcd.print("Spike went of :(");
  digitalWrite(GREEN_PIN, 0);
  digitalWrite(YELLOW_PIN, 0);
  digitalWrite(RED_PIN, 1);
  tone(BUZZER_PIN, BUZZER_FREQ * 4, 2000);
}

inline void displayDiffused()
{
  lcd.setCursor(0, 0);
  static char buf[17] = "Disarmed:      s";
  dtostrf(timer / 1000000. * TICK, 6, 2, buf+9);
  lcd.print(buf);
  digitalWrite(GREEN_PIN, 1);
  digitalWrite(YELLOW_PIN, 0);
  digitalWrite(RED_PIN, 0);
  noTone(BUZZER_PIN);
}

inline void validateCode()
{
  switch (state)
  {
  case INIT:
    if (!strcmp(currentCode, ARM_CODE))
    {
      state = ARMED;
      displayArmed();
      timer = RUN_TIME;
      Timer1.start();
    }
    break;
  case ARMED:
    if (!strcmp(currentCode, DIFFUSE_CODE))
    {
      state = DISARMED;
      displayDiffused();
      Timer1.stop();
    }
    break;
  case BLOWN:
  case DISARMED:
  case ENDED:
    if (!strcmp(currentCode, RESET_CODE))
    {
      state = INIT;
      displayInitialized();
    }
  }
}

inline void lcdUpdateCode()
{
  lcd.setCursor(6, 1);
  for (int i = 0; i < CODE_LENGTH; i++)
    lcd.print(' ');
  lcd.setCursor(6, 1);
  lcd.print(currentCode);
}

void keypadEventHandler(KeypadEvent key)
{
  switch (keypad.getState())
  {
  case RELEASED:
    switch (key)
    {
    case '#':
      currentCode[0] = '\0';
      break;
    case '*':
      validateCode();
      currentCode[0] = '\0';
      break;
    default:
      for (int i = 0; i < CODE_LENGTH; i++)
      {
        if (currentCode[i] != '\0')
          continue;
        currentCode[i] = key;
        currentCode[i + 1] = '\0';
        break;
      }
    }
    lcdUpdateCode();
    break;
  }
}

void timerISR()
{
  timer--;
  if (!timer)
  {
    state = BLOWN;
    Timer1.stop();
  }
}