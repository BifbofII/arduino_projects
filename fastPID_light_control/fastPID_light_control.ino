// ┌──────────────────────────────────────────────────┐
// │ Main source file for control systems project     │
// │ Controlling light intensity at a working surface │
// └──────────────────────────────────────────────────┘

// Import libraries
#include "Adafruit_NeoPixel.h"
#include "Encoder.h"
#include "FastPID.h"
#include "TimerOne.h"

// Debug settings
#define DEBUG
#ifdef DEBUG
#undef RECORDING
#ifdef RECORDING
#define RECORDING_LEN (180)                      // Recording length in samples
volatile short recordedValues[3][RECORDING_LEN]; // All the recorded data
volatile boolean recording = false;
volatile int recordingCount = 0;
volatile boolean recordingFinished = false;
#endif
#endif

// Pin values
#define PIN_LIGHT_SENSOR (A0)
#define PIN_LED_RING (5)
#define PIN_LED (6)
#define PIN_ROTARY_1 (3)
#define PIN_ROTARY_2 (2)
#define PIN_SWITCH (4)

// Configuration Parameters
// Program Flow
const int LOOP_DELAY = 200; // Delay time in the loop function in ms
#define SAMPLE_TIME 200000  // Sample time in us
// LED ring
#define RING_LED_NUM (12)
const int RING_OFF_TIME = 4000;
const int RING_OFF_COUNT = RING_OFF_TIME / LOOP_DELAY;
// Rotary
const int ROTARY_MAX_VALUE = 200; // Maximum value for the rotary encode
const int ROTARY_MIN_VALUE = 0;   // Minimum value for the rotary encode
// Setpoint
const int SETPOINT_MAX = 1023; // Maximum for setpoint
// Sensor
#define SENS_FLOATING_AVG_LEN (3)
// LED
#define LED_FLOATING_AVG_LEN (3)
// Ziegler Niechols
const double KR = .38;
const double TU = 3.2;
// Controller
const double KP = .45 * KR;
const double KI = KR / (.85 * TU);
const double KD = 0;
/* const double KP = .41; */
/* const double KI = 0; */
/* const double KD = 0; */

// Global variables
volatile int currentBrightness; // The brightness value of the sensor
volatile int setPoint;          // The set point for the controller
volatile int ledPower;          // The set point for the controller
boolean closedLoop = true; // Wether the control is in open or closed loop mode

// Global objects
// Encoder
Encoder rotary(PIN_ROTARY_1, PIN_ROTARY_2);
// LED ring
Adafruit_NeoPixel ring(RING_LED_NUM, PIN_LED_RING);
// PID controller
FastPID PID(KP, KI, KD, 1000000 / SAMPLE_TIME, 8, false);

// Color definitions for LED ring
const uint32_t BLACK = Adafruit_NeoPixel::Color(0, 0, 0);
const uint32_t GREEN = Adafruit_NeoPixel::Color(0, 255, 0);
const uint32_t RED = Adafruit_NeoPixel::Color(255, 0, 0);
const uint32_t BLUE = Adafruit_NeoPixel::Color(0, 0, 255);
const uint32_t YELLOW = Adafruit_NeoPixel::Color(255, 255, 0);
const uint32_t MAGENTA = Adafruit_NeoPixel::Color(255, 0, 255);
const uint32_t CYAN = Adafruit_NeoPixel::Color(0, 255, 255);
const uint32_t ORANGE = Adafruit_NeoPixel::Color(255, 128, 0);

const uint32_t COLOR_MATRIX[RING_LED_NUM] = {GREEN,  GREEN,  GREEN,  YELLOW,
                                             YELLOW, YELLOW, ORANGE, ORANGE,
                                             ORANGE, RED,    RED,    RED};

// Function prototypes
void timerISR();
void powerLed(int);
int floatingAverage(int, int, int *);
void showValue(double, double);
void resetLEDs();
uint32_t dimColor(uint32_t, double);
#ifdef DEBUG
void startRecording();
void stopRecording();
boolean recordData();
#endif

// Setup function
void setup() {
  // Setup Pins
  pinMode(PIN_LIGHT_SENSOR, INPUT);
  pinMode(PIN_LED_RING, OUTPUT);
  pinMode(PIN_LED, OUTPUT);
  pinMode(PIN_ROTARY_1, INPUT);
  pinMode(PIN_ROTARY_2, INPUT);
  pinMode(PIN_SWITCH, INPUT_PULLUP);

  // Setup LED ring
  ring.begin();
  ring.show();

  // Setup controller
  PID.setOutputRange(0, 255);
  if (PID.err())
    Serial.println("There is a configuration error!");

  // Decide if closed or open loop
  if (!digitalRead(PIN_SWITCH))
    closedLoop = !closedLoop;

  // Timer for PID sampling
  Timer1.initialize(SAMPLE_TIME);
  Timer1.attachInterrupt(timerISR);
  Timer1.stop();

#ifdef DEBUG
  // Serial for debugging
  Serial.begin(9600);
#endif
}

// Periodically called function
void loop() {
#ifdef DEBUG
  static boolean verbose = false;
#endif
  static int oldSwitchState = 1;   // Old state of switch to detect edges
  static boolean turnedOn = false; // Switch if lamp is turned on
  static double oldRotaryValue =
      0; // Old state of encoder to count loops since change
  static int encoderCount = 0; // Loops since last encode change

#ifdef DEBUG
  char incomingByte;
  // Read commands over serial
  if (Serial.available() > 0) {
    // read the incoming byte:
    incomingByte = Serial.read();

    switch (incomingByte) {
    // Switch mode
    case 'm':
    case 'M':
      closedLoop = !closedLoop;
      if (!closedLoop)
        Timer1.stop();
      else
        Timer1.start();
      Serial.print("Switched mode to ");
      Serial.println(closedLoop ? "closed loop" : "open loop");
      break;
    // Switch verbosity
    case 'v':
    case 'V':
      verbose = !verbose;
      Serial.print("Switched verbosity ");
      Serial.println(verbose ? "on" : "off");
      break;
    // Toggle on off
    case 'o':
    case 'O':
      turnedOn = !turnedOn;
      if (closedLoop) {
        if (turnedOn)
          Timer1.start();
        else
          Timer1.stop();
      }

      if (!turnedOn)
        digitalWrite(PIN_LED, false);
      Serial.print("Switched lamp ");
      Serial.println(turnedOn ? "on" : "off");
      break;
    // Increase Setpoint
    case '+':
      rotary.write(rotary.read() + 10);
      Serial.println("Turned brightness up");
      break;
    // Decrease Setpoint
    case '-':
      rotary.write(rotary.read() - 10);
      Serial.println("Turned brightness down");
      break;
    // Step Setpoint
    case 's':
    case 'S':
      rotary.write(ROTARY_MAX_VALUE * .6);
      Serial.println("Set brightness to 60");
      break;
#ifdef RECORDING
    // Record data
    case 'r':
    case 'R':
      startRecording();
      break;
#endif
    }
  }
#endif

  if (!digitalRead(PIN_SWITCH) && oldSwitchState) {
    if (turnedOn) {
      Timer1.stop();
      digitalWrite(PIN_LED, false);
    } else if (closedLoop)
      Timer1.start();
    turnedOn = !turnedOn;
    encoderCount = 0;
  }
  oldSwitchState = digitalRead(PIN_SWITCH);

  // Get set value from encoder
  if (rotary.read() > ROTARY_MAX_VALUE)
    rotary.write(ROTARY_MAX_VALUE);
  else if (rotary.read() < ROTARY_MIN_VALUE)
    rotary.write(ROTARY_MIN_VALUE);
  setPoint = float(rotary.read()) / ROTARY_MAX_VALUE * SETPOINT_MAX;

  // Output power to LED
  if (!closedLoop)
    powerLed(float(setPoint) / SETPOINT_MAX * 255 * turnedOn);

  // Show set point on LED ring
  showValue(setPoint, encoderCount < RING_OFF_COUNT ? (turnedOn ? 1 : .2) : 0);

#ifdef RECORDING
  if (recordingFinished)
    stopRecording();
#endif

#ifdef DEBUG
  if (verbose) {
    Serial.print("Turned On: ");
    Serial.print(turnedOn);
    Serial.print(", Brightness: ");
    Serial.print(currentBrightness);
    Serial.print(", Set Point: ");
    Serial.print(setPoint);
    Serial.print(", Power: ");
    Serial.println(ledPower);
  }
#endif

  // Count loops since last encoder change for LED ring timeout
  encoderCount++;
  if (oldRotaryValue != rotary.read())
    encoderCount = 0;
  else if (encoderCount >= RING_OFF_COUNT)
    encoderCount = RING_OFF_COUNT;
  oldRotaryValue = rotary.read();

  delay(LOOP_DELAY);
}

void timerISR() {
  static int floatingAvgSensArr[SENS_FLOATING_AVG_LEN]; // Array for calculation
                                                        // of floating average
                                                        // for sensor
  static int
      floatingAvgLedArr[LED_FLOATING_AVG_LEN]; // Array for calculation
                                               // of floating average for LED
#if SENS_FLOATING_AVG_LEN > 1
  currentBrightness = floatingAverage(
      analogRead(PIN_LIGHT_SENSOR), SENS_FLOATING_AVG_LEN, floatingAvgSensArr);
#else
  currentBrightness = analogRead(PIN_LIGHT_SENSOR);
#endif
  setPoint = float(rotary.read()) / ROTARY_MAX_VALUE * SETPOINT_MAX;

#if LED_FLOATING_AVG_LEN > 1
  ledPower = floatingAverage(PID.step(setPoint, currentBrightness),
                             LED_FLOATING_AVG_LEN, floatingAvgLedArr);
#else
  ledPower = PID.step(setPoint, currentBrightness);
#endif
  powerLed(ledPower);

#ifdef RECORDING
  if (recording)
    recordingFinished = !recordData();
#endif
}

// Write a light value to the power LED with a power between 0 and 255
void powerLed(int power) {
  if (power > 255)
    power = 255;
  else if (power < 0)
    power = 0;
  analogWrite(PIN_LED, power);
}

#if LED_FLOATING_AVG_LEN > 1 || SENS_FLOATING_AVG_LEN > 1
// Calculate the floating average of N values
int floatingAverage(int val, int N, int *oldVals) {
  long sum = 0;

  for (int i = 0; i < N; i++)
    oldVals[N - i - 1] = oldVals[N - i - 2];
  oldVals[0] = val;

  for (int i = 0; i < N; i++)
    sum += oldVals[i];
  return sum / N;
}
#endif

// Show a value between 0 and SETPOINT_MAX on the LED ring
void showValue(double value, double brightness) {
  double ledVal = value / SETPOINT_MAX * RING_LED_NUM;
  int fullOnNum = ledVal;
  double lastInt = ledVal - fullOnNum;
  int i;

  ring.setBrightness(brightness * 255);

  resetLEDs();

  for (i = 0; i < fullOnNum; i++)
    ring.setPixelColor(i, COLOR_MATRIX[i]);
  ring.setPixelColor(i, dimColor(COLOR_MATRIX[i], lastInt));

  ring.show();
}

// Reset all LEDs to black
void resetLEDs() {
  for (int i = 0; i < RING_LED_NUM; i++)
    ring.setPixelColor(i, BLACK);
}

// Dim a color for one pixel with a dimming value between 0 and 1
uint32_t dimColor(uint32_t color, double dimValue) {
  if (dimValue < 0)
    dimValue = 0;
  if (dimValue > 1)
    dimValue = 1;

  uint8_t red = ((color >> 16) & 0xFF) * dimValue;
  uint8_t green = ((color >> 8) & 0xFF) * dimValue;
  uint8_t blue = ((color)&0xFF) * dimValue;

  return Adafruit_NeoPixel::Color(red, green, blue);
}

#ifdef RECORDING
// Start recording data
void startRecording() {
  Serial.println("Starting recording of data");
  recordingCount = 0;
  recording = true;
}

// Stop recording data
void stopRecording() {
  recording = false;
  recordingFinished = false;
  Serial.println("Stopped recording of data");
  Serial.println("Dumping Data");
  Serial.println("--- CSV Starting here ---");

  Serial.println("Timestamp,Setpoint,LED,Brightness");
  delay(30);
  for (int i = 0; i < RECORDING_LEN; i++) {
    Serial.print(long(i) * SAMPLE_TIME / 1000);
    delay(5);
    Serial.print(", ");
    Serial.print(recordedValues[0][i]);
    delay(5);
    Serial.print(", ");
    Serial.print(recordedValues[1][i]);
    delay(5);
    Serial.print(", ");
    Serial.println(recordedValues[2][i]);
    delay(5);
  }
  Serial.println("--- CSV Ending here ---");
}

// Fucntion for recording data
boolean recordData() {
  if (recordingCount >= RECORDING_LEN)
    return false;

  recordedValues[0][recordingCount] = short(setPoint);
  recordedValues[1][recordingCount] = short(ledPower);
  recordedValues[2][recordingCount] = analogRead(PIN_LIGHT_SENSOR);

  recordingCount++;

  return true;
}
#endif
