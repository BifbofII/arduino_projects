#include <FastLED.h>
#include <Keypad.h>
#include <MFRC522.h>
#include <SPI.h>

// Pin configuration
const byte RFID_RST = 9;                        // RFID reset pin
const byte RFID_SS = 10;                        // RFID data pin
const byte KEYPAD_ROW_PINS[] = {A5, 8, 7, 6};   // Keypad row pins
const byte KEYPAD_COLUMN_PINS[] = {5, 4, 3, 2}; // Keypad column pins
const byte RELAIS_PIN = A0;                     // Pin for the relais
const byte LED_PIN = A2;                        // Data pin for the LEDs

// Game configuration
const byte RFID_UID[] = {0xC6, 0xCA, 0xED, 0x93}; // RFID UID to use
const char START_CODE[] = {'A', '4', 'C', '3', '0', '*'};
const byte START_CODE_LENGTH = sizeof(START_CODE) / sizeof(char);

// RFID configuration
MFRC522 mfrc522(RFID_SS, RFID_RST); // MFRC522 instance
MFRC522::MIFARE_Key key;            // Key of the RFID reader

// Keypad configuration
const byte KEYPAD_ROWS =
    sizeof(KEYPAD_ROW_PINS) / sizeof(byte); // Number of rows for the keypad
const byte KEYPAD_COLUMNS = sizeof(KEYPAD_COLUMN_PINS) /
                            sizeof(byte); // Number of columns for the keypad
const char KEYPAD_NAMES[KEYPAD_ROWS][KEYPAD_COLUMNS] = {
    {'1', '2', '3', 'A'},
    {'4', '5', '6', 'B'},
    {'7', '8', '9', 'C'},
    {'*', '0', '#', 'D'}}; // The names of the keys on the keypad
Keypad customKeypad =
    Keypad(makeKeymap(KEYPAD_NAMES), KEYPAD_ROW_PINS, KEYPAD_COLUMN_PINS,
           KEYPAD_ROWS, KEYPAD_COLUMNS); // Initialize keypad

// LED configuration
const byte LED_LENGTH = 20;
CRGB leds[LED_LENGTH];

// State variables
bool rfidAccess = false;
byte startCodePosition = 0;

void setup() {
  Serial.begin(9600); // Initialize serial communication
  while (!Serial)
    ;                 // Wait until serial communication is up
  SPI.begin();        // Initialize SPI bus
  mfrc522.PCD_Init(); // Initialize mfrc522 card
  Serial.println(F("Defibrillator for little helper"));

  // Prepare key for the RFID reader (FFFFFFFFFFFFh is the factory standard UID)
  for (byte i = 0; i < 6; i++)
    key.keyByte[i] = 0xFF;

  // Set pin modes
  pinMode(RELAIS_PIN, OUTPUT);

  // Set up LEDs
  FastLED.addLeds<WS2812B, LED_PIN, GRB>(leds, LED_LENGTH);
  for (byte i = 0; i < LED_LENGTH; i++)
    leds[i] = CRGB::Black;
  FastLED.show();
}

void revive() {
  for (byte n = 0; n < 2; n++) {
    for (byte i = 0; i < LED_LENGTH; i++)
      leds[i] = CRGB::Black;
    FastLED.show();
    for (byte i = 0; i < LED_LENGTH; i++) {
      delay(200);
      leds[i] = CRGB::Cyan;
      FastLED.show();
    }
  }
  digitalWrite(RELAIS_PIN, HIGH);
}

void showStatus() { // Show status on the LEDs
  if (rfidAccess)
    for (byte i = 0; i < LED_LENGTH; i++)
      if (startCodePosition > i)
        leds[i] = CRGB::Green;
      else if (i < START_CODE_LENGTH)
        leds[i] = CRGB::Orange;
      else
        leds[i] = CRGB::Black;
  if (startCodePosition == START_CODE_LENGTH)
    for (byte i = 0; i < LED_LENGTH; i++)
      leds[i] = CRGB::Green;
  FastLED.show();
}

void loop() {
  // return if new RFID card is present
  if (mfrc522.PICC_IsNewCardPresent() && mfrc522.PICC_ReadCardSerial() &&
      !rfidAccess) {
    Serial.println("Recognized RFID tag");
    // Dump RFID UID and check if it's the correct one
    bool correct = true;
    Serial.print(F("Card UID: "));
    for (byte i = 0; i < mfrc522.uid.size; i++) {
      Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
      Serial.print(mfrc522.uid.uidByte[i], HEX);
      if (mfrc522.uid.uidByte[i] != RFID_UID[i])
        correct = false;
    }
    Serial.println();
    if (!correct) {
      Serial.println("Wrong RFID tag");
      delay(100);
      return;
    }
    Serial.println("RFID access granted");
    rfidAccess = true;
    delay(100);
  }

  if (rfidAccess) {
    char keyPressed = customKeypad.getKey();

    if (keyPressed == START_CODE[startCodePosition]) {
      startCodePosition++;
      Serial.print("Start code position: ");
      Serial.println(startCodePosition);
    } else if (keyPressed) {
      startCodePosition = 0;
      Serial.print("Start code position: ");
      Serial.println(startCodePosition);
    }

    showStatus();

    if (startCodePosition == START_CODE_LENGTH) {
      Serial.println("Little helper successfully revived");
      delay(2000);
      revive();
    }
  }

  delay(100);
}
